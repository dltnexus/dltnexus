### DLT Nexus VISION AND PURPOSE: 

The purpose of the DLT Nexus Project is to function as a collaborative incubator environment for individuals, groups, and organizations to discover, cultivate, and share their unique gifts with the rest of the world. This initial vision statement is carried over intact from the P99Power Project which is being deferred for the time being in order to concentrate assets into building the foundation around the Distributed Ledger Technology Hub.

### CORE PRINCIPALS:

- BE AS YOU WISH TO SEEM
- SHARE YOUR UNIQUE GIFT
- YOU CANNOT POUR FROM AN EMPTY CUP
- LIFE IS A MARATHON NOT A SPRINT
- ATTITUDE OF ABUNDANCE
- DO NO HARM
- ESPRITE DE CORPS
