# DLT NEXUS

## Distributed Ledger Technology Hub
### Innovation. Integration. Incubation.

This is the beginning of the DLT NEXUS Project. The vision is an ambitious one. We intend to create a distributed autonomous organization from scratch formatted around the Git and GitHub framework for future integration into the Nexus application framework.

This repo will chronicle the painful and iterative process of turning a dream into a reality and building a foundation strong enough to stand the test of time. 

Our hope is that the generations that follow will appreciate the hard work and humble beginnings as they marvel at the powerful and polished results we achieve in the bright and beautiful future that lies ahead.

Be as you wish to seem.
